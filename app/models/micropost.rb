class Micropost < ApplicationRecord
  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_one_attached :image
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validates :image,   content_type: { in: %w[image/jpeg image/gif image/png], message: "must be a valid image format" },
                      size:         { less_than: 5.megabytes, message: "should be less than 5MB" }

  #リサイズされた画像を返す
  def display_image(width =500,height = 500)
    image.variant(resize_to_limit: [width, height])
  end

  #投稿のいいね数を返す
  def likes_count 
    self.likes.count
  end
  
  #引数のユーザーが投稿をいいねしているかどうか
  def islike?(user_id)
    self.likes.where(user_id: user_id).any?
  end
end
