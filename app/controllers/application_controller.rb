class ApplicationController < ActionController::Base
    include SessionsHelper
    before_action :set_search
    
    def set_search
      @search = User.ransack(params[:q])
      @search_users = @search.result
    end

    private

    # ユーザーのログインを確認する
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
end
