class SearchsController < ApplicationController
    before_action :logged_in_user, only: [:index]

    def index
        @users = @search_users.paginate(page: params[:page]) if !@search_users.nil?
    end
end
