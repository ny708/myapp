class LikesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :show]
  
  def show 
    @user = User.find(params[:id])
    @microposts  = current_user.likes_feed.paginate(page: params[:page])
  end

  def create 
    @like = Like.new(user_id:current_user.id, micropost_id:params[:micropost_id])
    @like.save
    @micropost = Micropost.find_by(id: params[:micropost_id])
    respond_to do |format|
      format.js
    end
  end

  def destroy 
    @like = Like.find_by(user_id:current_user.id, micropost_id:params[:micropost_id])
    @like.destroy
    @micropost = Micropost.find_by(id: params[:micropost_id])
    respond_to do |format|
      format.js
    end
  end
end
