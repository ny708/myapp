Rails.application.routes.draw do
 
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'
  get 'users/new'
  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :users do
    collection do
      get 'search'
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy, :show]
  resources :relationships,       only: [:create, :destroy]

  post 'likes/:micropost_id/create', to: 'likes#create'
  delete 'likes/:micropost_id/destroy', to: 'likes#destroy'
  resources :likes, only: [:show]
  resources :comments,          only: [:create, :destroy]
  resources :searchs,           only: [:index]
  resources :maps,              only: [:index]
end
