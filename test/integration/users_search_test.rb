require 'test_helper'

class UsersSearchTest < ActionDispatch::IntegrationTest
  def setup
    @user     = users(:michael)
  end

  test "search user test" do
    
    log_in_as(@user)

    # 検索ページの初期表示確認
    get searchs_path
    assert_template 'searchs/index'
    assert_select 'div.pagination'
    assert_select 'form#user_search'

    # ユーザーの検索結果を確認
    get searchs_path, params:{ q:{ name_cont: "lana" } }
    assert_template 'searchs/index'
    searched_users = assigns(:users)
    first_user = searched_users.first
    assert_select 'a[href=?]', user_path(first_user), text: first_user.name
    assert_select 'div.pagination', count: 0

  end
end
