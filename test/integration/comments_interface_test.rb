require 'test_helper'

class CommentsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @comment_user = users(:michael)
    @non_comment_user = users(:archer)
    @micropost_user = users(:lana)
    @micropost = @micropost_user.microposts.build(content: "test content")
    @micropost.save
  end

  test "comment interface test" do
    log_in_as(@comment_user)
    get micropost_path(@micropost)
    
    # 無効な送信
    assert_no_difference 'Comment.count' do
      post comments_path, params: { comment: { content: "" }, micropost_id: @micropost.id}
    end

    # 有効な送信
    content = "I like your post"
    assert_difference 'Comment.count', 1 do
      post comments_path, params: { comment: { content: content }, micropost_id: @micropost.id }
    end
    assert_redirected_to micropost_path(@micropost)
    follow_redirect!
    assert_match content, response.body

    # 違うユーザーでログインし削除リンクがないことを確認
    log_in_as(@non_comment_user)
    get micropost_path(@micropost)
    assert_select 'a', text: 'delete', count: 0

    # コメントを削除する
    log_in_as(@comment_user)
    get micropost_path(@micropost)
    assert_select 'a', text: 'delete'
    delete_comment = @comment_user.comments.where(micropost_id: @micropost.id).last
    assert_difference 'Comment.count', -1 do
      delete comment_path(delete_comment)
    end
  end
end
