require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
    def setup
      @user = users(:michael)
    end
    
    test "link test" do
      get root_path
      assert_template 'static_pages/home'
      assert_select "a[href=?]", root_path, count: 2
      assert_select "a[href=?]", help_path
      assert_select  "a[href=?]",login_path,count:2
      get signup_path
      assert_template 'users/new'
      
      log_in_as(@user)
      get root_path
      assert_template 'static_pages/home'
      assert_select "a[href=?]", root_path, count: 3
      assert_select "a[href=?]", help_path
      assert_select "a[href=?]", users_path
      assert_select "a[href=?]", user_path(@user)
      assert_select "a[href=?]", edit_user_path(@user)
      assert_select "a[href=?]", logout_path
      assert_select "a[href=?]", searchs_path

    end
  
end