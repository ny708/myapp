require 'test_helper'

class LikesFeedTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
    @other_user = users(:lana)
    @micropost = @other_user.microposts.build(content: "test content")
    @micropost.save
    @like = @micropost.likes.build(user_id: @user.id)
    @like.save
  end

  test "likes feed" do

    # いいねした投稿がないユーザー
    log_in_as(@other_user)
    assert_equal @other_user.likes_feed.count, 0

    # いいねした投稿があるユーザー
    log_in_as(@user)
    assert_equal @user.likes_feed.count, 1 
    get like_path(@user)
    assert_template 'likes/show'
    assert_match @micropost.content, response.body
  end
end
