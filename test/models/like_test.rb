require 'test_helper'

class LikeTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @micropost = @user.microposts.build(content: "test content")
    @micropost.save
    @like = @micropost.likes.build(user_id: @user.id)
  end

  test "should be valid" do
    assert @like.valid?
  end

  test "user_id should be present" do
    @like.user_id = nil
    assert_not @like.valid?
  end

  test "micropost_id should be present" do
    @like.micropost_id = nil
    assert_not @like.valid?
  end
end
