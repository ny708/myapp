require 'test_helper'

class SearchsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user       = users(:michael)
   end

  test "should redirect index when not logged in user" do
    get searchs_path
    assert_redirected_to login_url
  end
end
