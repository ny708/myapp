require 'test_helper'

class LikesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @micropost = microposts(:tone)
    @other_micropost = @user.microposts.build(content: "for destroy")
    @other_micropost.save 
    @other_micropost.likes.build(user_id: @user.id).save
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Like.count' do
      post "/likes/#{@micropost.id}/create", params: { like: { user_id: @user.id, micropost_id: @micropost.id} }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Like.count' do
      delete "/likes/#{@other_micropost.id}/destroy", params: { like: { user_id: @user.id, micropost_id: @other_micropost.id} }
    end
    assert_redirected_to login_url
  end

  test "should redirect show when not logged in" do
    get like_path(@user)
    assert_redirected_to login_url
  end

end
