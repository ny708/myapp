require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
 
  def setup
    @comment = comments(:ten_minutes_ago)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Comment.count' do
      post comments_path, params: { comment: { content: "not logged in" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Comment.count' do
      delete comment_path(@comment)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy for wrong comment" do
    log_in_as(users(:archer))
    comment = comments(:ten_minutes_ago)
    assert_no_difference 'Comment.count' do
      delete micropost_path(comment)
    end
    assert_redirected_to root_url
  end
end
